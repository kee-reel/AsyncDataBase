#include "database.h"
#include <QtConcurrent/QtConcurrent>
static QStringList s_possibleDriverNames = {"QSQLITE", "SQLITECIPHER"};
static QThreadStorage<DatabaseConnection *> databaseConnections;

DataBase::DataBase() :
	QObject(nullptr),
	PluginBase(this),
	m_pool(new QThreadPool)
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IAsyncDataBase), this}
	});
	m_pool->setMaxThreadCount(1);
	m_pool->setExpiryTimeout(120000);
}

DataBase::~DataBase()
{
}

void DataBase::onReady()
{
}

QSqlDatabase DataBase::currentDatabase()
{
	if (!databaseConnections.hasLocalData()) {
		databaseConnections.setLocalData(new DatabaseConnection());
	}
	return databaseConnections.localData()->database();
}

QSharedPointer<IQuery> DataBase::executeQueryAsync(const QString &queryStr)
{
	return QSharedPointer<Query>(new Query(m_pool, queryStr));
}

QSharedPointer<IQuery> DataBase::executeQueryAsync(const QString &queryStr, const QList<QString> &valuePlaceholders, const QList<QVariant> &values)
{
	return QSharedPointer<Query>(new Query(m_pool, queryStr, valuePlaceholders, values));
}

Query::Query(QWeakPointer<QThreadPool> pool, const QString &query) :
	QObject(),
	m_pool(pool),
	m_queryStr(query),
	m_uuid(QUuid::createUuid())
{

}

Query::Query(QWeakPointer<QThreadPool> pool, const QString &query, const QList<QString>& valuePlaceholders, const QList<QVariant>& values) :
	QObject(),
	m_pool(pool),
	m_queryStr(query),
	m_valuePlaceholders(valuePlaceholders),
	m_values(values),
	m_uuid(QUuid::createUuid())
{
}

void Query::exec()
{
	connect(&m_futureWatcher, SIGNAL(finished()), this, SLOT(onFinished()));
	m_future = QtConcurrent::run(m_pool.toStrongRef().data(), [this]() -> QueryReturnType {
		DatabaseConnection conn;
		QSqlQuery query(conn.database());
		query.prepare(m_queryStr);
		for(int i = 0; i < m_valuePlaceholders.count(); ++i)
		{
			QFlags<QSql::ParamTypeFlag> flag = (m_values.at(i).typeId() == QMetaType::Type::QByteArray) ? QSql::In | QSql::Binary : QSql::In;
			query.bindValue(m_valuePlaceholders.at(i), m_values.at(i), flag);
		}
		QueryValuesReturnType result;
		if(!query.exec())
		{
			qDebug() << "Error in execution query" << query.lastQuery() << "Error:" << query.lastError();
		}
		else
		{
			QVariant rowValue;
			QVector<QVariant> rowValues;
			while(query.next())
			{
				int i = 0;
				while(true)
				{
					rowValue = query.value(i);
					if(rowValue.isValid())
					{
						if(rowValues.size() <= i)
						{
							rowValues.append(rowValue);
						}
						else
						{
							rowValues[i] = rowValue;
						}
					}
					else
					{
						break;
					}
					++i;
				}
				result.append(rowValues);
			}
		}
		return QueryReturnType(query.lastInsertId().toInt(), result);
	});
	m_futureWatcher.setFuture(m_future);
}

void Query::onFinished()
{
	m_result = m_future.result();
	emit completed(m_uuid);
}

QUuid Query::uuid()
{
	return m_uuid;
}

QObject *Query::object()
{
	return this;
}

int Query::lastInsertId()
{
	if(m_future.isFinished())
	{
		return m_result.first;
	}
	else
	{
		return m_future.result().first;
	}
}

QueryValuesReturnType Query::values()
{
	if(m_future.isFinished())
	{
		return m_result.second;
	}
	else
	{
		return m_future.result().second;
	}
}
